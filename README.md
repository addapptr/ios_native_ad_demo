# AddApptr native app example based on the Yodel Sample App - iOS of Flurry (Yahoo)


- AATKit used in version 2.61.0


## Native Ads example
- The native ad example app created by Flurry, has been adapted in order use the AddApptr SDK instead of 
the FlurryAds SDK only.
While the AddApptr SDK does support native ads by Flurry, additional ad networks are also supported.
Hence, you'll find native ads of multiple other ad networks in the feed view of the application.

- [AdManager](YodelSample/Helpers/AdManager.m): The AdManager class is a good example of how ads can be controlled
in a central class, instead of being scattered throughout an application. The AdManager makes sure to load multiple
native ads at once, which makes them available through caching instead of loading them only on demand.
The original Yodel Sample App can be found [here](https://github.com/flurry/YodeliOSApp)